﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace ContinuumDump.Common
{
    public class Template: IEnumerable
    {
        private Dictionary<string, ParamValue> _parameters = new Dictionary<string, ParamValue>();
        private ArrayList _lines = new ArrayList();

        public Template(string FilePath, string pattern)
        {
            _parameters.Clear();
            _lines.Clear();
            if (File.Exists(FilePath))
            {
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        foreach (Match match in Regex.Matches(line, pattern))
                        {
                            if (!_parameters.ContainsKey(match.Value))
                                _parameters.Add(match.Value, new ParamValue(match.Value));
                        }
                        _lines.Add(line);
                    }
                }
            }
        }

        public FlowDocument CreateTemplateDocument(string pattern)
        {
            FlowDocument document = new FlowDocument();

            Paragraph paragraph = new Paragraph();
            foreach (string line in _lines)
            {
                MatchCollection matchReturn = Regex.Matches(line, pattern);
                if (matchReturn.Count != 0)
                {
                    int index = 0;
                    foreach (Match match in matchReturn)
                    {
                        paragraph.Inlines.Add(new Run(line.Substring(index, match.Index - index)));
                        index = match.Index + match.Length;

                        TextBlock textBlock = new TextBlock();
                        _parameters[match.Value].TextControl.Add(textBlock);
                        textBlock.Text = _parameters[match.Value].Value;
                        textBlock.Background = Brushes.Green;
                        paragraph.Inlines.Add(textBlock);
                    }
                    paragraph.Inlines.Add(new Run(line.Substring(index, line.Length - index)));
                }
                else
                {
                    paragraph.Inlines.Add(new Run(line));
                }
                paragraph.Inlines.Add(new LineBreak());
            }
            document.Blocks.Add(paragraph);

            return document;
        }

        public IEnumerator GetEnumerator()
        {
            return _lines.GetEnumerator();
        }
    }
}
